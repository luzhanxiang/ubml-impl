/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.externalapi.definition.temp.lcm;

import org.openatom.ubml.model.common.definition.entity.GspMetadata;

/**
 * The type DimensionExtendEntity
 *
 * @author: haozhibei
 */
public class DimensionExtendEntity {

    public void setBasicMetadataId(String id) {

    }

    public void setBasicMetadataCode(String code) {

    }

    public void setBasicMetadataNamespace(String space) {

    }

    public void setBasicMetadataTypeStr(String type) {

    }

    public void setExtendMetadataEntity(GspMetadata metadata) {

    }

    public void setFirstDimension(String dimension) {

    }

    public void setFirstDimensionCode(Object code) {

    }

    public void setFirstDimensionName(Object name) {

    }

    public void setSecondDimension(String dimension) {

    }

    public void setSecondDimensionCode(Object code) {

    }

    public void setSecondDimensionName(Object name) {

    }
}
