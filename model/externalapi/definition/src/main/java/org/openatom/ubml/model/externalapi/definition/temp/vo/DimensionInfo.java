/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.externalapi.definition.temp.vo;

/**
 * The type DimensionInfo
 *
 * @author: haozhibei
 */
public class DimensionInfo {
    public String getFirstDimension() {
        return null;
    }

    public String getSecondDimension() {
        return null;
    }

    public Object getFirstDimensionCode() {
        return null;
    }

    public Object getFirstDimensionName() {
        return null;
    }

    public Object getSecondDimensionCode() {
        return null;
    }

    public Object getSecondDimensionName() {
        return null;
    }
}
