package org.opeantom.ubml.externalapi.runtime.api;


import java.util.List;

/**
 * The endpoint info
 *
 * @author haozhibei
 */
public class EndpointInfo {

    private String path;

    private List<Object> services;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Object> getServices() {
        return services;
    }

    public void setServices(List<Object> services) {
        this.services = services;
    }
}
