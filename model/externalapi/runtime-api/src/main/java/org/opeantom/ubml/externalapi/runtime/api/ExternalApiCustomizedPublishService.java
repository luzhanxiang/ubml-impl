package org.opeantom.ubml.externalapi.runtime.api;

import java.util.List;

/**
 * ExternalApiCustomizedPublishService
 *
 * @author haozhibei
 */
public interface ExternalApiCustomizedPublishService {
    /**
     * 编译并发布服务
     *
     * @param metadataId
     */
    void publish(String metadataId);

    /**
     * 移除发布的服务
     *
     * @param metadataId
     */
    void unpublish(String metadataId);

    /**
     * 获取全部的已发布的信息
     * TODO：另外的职责，新建接口
     *
     * @return
     */
    List<PublishedExternalApiInfo> getAllPublishedServices();
}
