package org.opeantom.ubml.externalapi.runtime.api;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import org.openatom.ubml.common.spring.SpringUtils;

/**
 * 代码编译调用的Client，已被EApi生成代码引用，请勿修改
 *
 * @ClassName: ServiceInvoker
 * @Author: Fynn Qi
 * @Date: 2020/2/4 10:02
 * @Version: V1.0
 */
public class ExternalApiInvokeAgent {

    public static Object invokeByJsonNode(String resourceType, String resourceId, String resourceOpId, List<JsonNode> paramList) {
        ExternalApiEngine externalApiEngine = SpringUtils.getBean(ExternalApiEngine.class);
        return externalApiEngine.invokeByJsonNode(resourceType, resourceId, resourceOpId, paramList);
    }

    public static Object invokeByObject(String resourceType, String resourceId, String resourceOpId, List<Object> paramList) {
        ExternalApiEngine externalApiEngine = SpringUtils.getBean(ExternalApiEngine.class);
        return externalApiEngine.invokeByObject(resourceType, resourceId, resourceOpId, paramList);
    }
}
