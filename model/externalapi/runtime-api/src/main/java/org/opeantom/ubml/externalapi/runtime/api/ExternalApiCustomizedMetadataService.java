package org.opeantom.ubml.externalapi.runtime.api;

import org.openatom.ubml.model.common.definition.entity.GspMetadata;

/**
 * 运行时元数据服务接口
 *
 * @ClassName: ExternalApiRuntimeCustomizationService
 * @Author: Fynn Qi
 * @Date: 2020/6/23 15:56
 * @Version: V1.0
 */
public interface ExternalApiCustomizedMetadataService {

    /**
     * 创建运行时EApi元数据
     *
     * @param voMetadata VO元数据
     * @return 元数据
     */
    GspMetadata create(GspMetadata voMetadata);

    /**
     * 根据依赖的VO元数据更新运行时Eapi元数据
     *
     * @param metadataId 更新的元数据ID
     * @param voMetadata VO元数据
     * @return 元数据
     */
    GspMetadata update(String metadataId, GspMetadata voMetadata);
}
