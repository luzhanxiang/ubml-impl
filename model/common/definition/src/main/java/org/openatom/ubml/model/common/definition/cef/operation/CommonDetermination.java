/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.operation;

import java.util.EnumSet;
import org.openatom.ubml.model.common.definition.cef.collection.DtmElementCollection;

/**
 * The Definition Of Common Determination
 *
 * @ClassName: CommonDetermination
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDetermination extends CommonOperation {
    private DtmElementCollection rqtElements;
    private EnumSet<ExecutingDataStatus> status = EnumSet.of(ExecutingDataStatus.forValue(0));

    public final EnumSet<ExecutingDataStatus> getGetExecutingDataStatus() {
        return status;
    }

    public final void setGetExecutingDataStatus(EnumSet<ExecutingDataStatus> value) {
        status = value;
    }

    public final DtmElementCollection getRequestElements() {
        if (rqtElements == null) {
            rqtElements = new DtmElementCollection();
        }
        return rqtElements;
    }

    public final void setRequestElements(DtmElementCollection value) {
        rqtElements = value;
    }
}