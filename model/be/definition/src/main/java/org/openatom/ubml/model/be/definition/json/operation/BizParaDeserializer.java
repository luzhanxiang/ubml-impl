/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizParameter;
import org.openatom.ubml.model.be.definition.operation.componentenum.BizCollectionParameterType;
import org.openatom.ubml.model.be.definition.operation.componentenum.BizParameterMode;
import org.openatom.ubml.model.be.definition.operation.componentenum.BizParameterType;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The  Josn Deserializer Of Biz Parameter
 *
 * @ClassName: BizParaDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizParaDeserializer<T extends BizParameter> extends JsonDeserializer<T> {
    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializePara(jsonParser);
    }

    public final T deserializePara(JsonParser jsonParser) {
        T op = createBizPara();

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(op, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);

        return op;
    }

    private void readPropertyValue(BizParameter para, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CommonModelNames.ID:
                para.setID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.ParamCode:
                para.setParamCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.ParamName:
                para.setParamName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.ParameterType:
                para.setParameterType(SerializerUtils.readPropertyValue_Enum(jsonParser, BizParameterType.class, BizParameterType.values()));
                break;
            case BizEntityJsonConst.Assembly:
                para.setAssembly(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.ClassName:
                para.setNetClassName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.JavaClassName:
                para.setClassName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.Mode:
                para.setMode(SerializerUtils.readPropertyValue_Enum(jsonParser, BizParameterMode.class, BizParameterMode.values()));
                break;
            case BizEntityJsonConst.ParamDescription:
                para.setParamDescription(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.CollectionParameterType:
                para.setCollectionParameterType(SerializerUtils.readPropertyValue_Enum(jsonParser, BizCollectionParameterType.class, BizCollectionParameterType.values()));
                break;
            default:
                if (!readExtendParaProperty(para, propName, jsonParser)) {
                    throw new RuntimeException(String.format("BizOperationDeserializer未识别的属性名：%1$s", propName));
                }
        }
    }

    protected boolean readExtendParaProperty(BizParameter op, String propName, JsonParser jsonParser) {
        return false;
    }

    protected abstract T createBizPara();

}
