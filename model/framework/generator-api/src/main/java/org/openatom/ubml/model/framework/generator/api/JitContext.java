/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.framework.generator.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openatom.ubml.model.common.definition.entity.GspMetadata;
import org.openatom.ubml.model.framework.api.RefCommonService;
import org.openatom.ubml.model.framework.definition.entity.GspProject;
import org.openatom.ubml.model.framework.definition.entity.MetadataProject;
import org.openatom.ubml.model.framework.generator.api.temp.ModuleGenerateInfo;
import org.openatom.ubml.model.framework.generator.api.temp.ModuleGenerator;
import org.openatom.ubml.model.framework.generator.api.temp.RefCommonServiceImp;

public class JitContext {
    RefCommonService refCommonService;

    private String gspProjectpath;
    private GspProject gspProjectInfo;
    private MetadataProject metadataProjectInfo;
    private Map<String, GspMetadata> metadataCache = new HashMap<>();

    private List<ModuleGenerateInfo> moduleInfoList;
    private ModuleGenerator apiModuleGenerator;
    private ModuleGenerator coreModuleGenerator;
    private GspMetadata metadata;
    private String codePath;
    private String resourcePath;
    private Map<String, Object> extendProperty = new HashMap<>();

    public JitContext() {
        this.refCommonService = new RefCommonServiceImp();
    }

    public String getGspProjectpath() {
        return gspProjectpath;
    }

    public void setGspProjectpath(String gspProjectpath) {
        this.gspProjectpath = gspProjectpath;
    }

    public GspProject getGspProjectInfo() {
        return gspProjectInfo;
    }

    public void setGspProjectInfo(GspProject gspProjectInfo) {
        this.gspProjectInfo = gspProjectInfo;
    }

    public MetadataProject getMetadataProjectInfo() {
        return metadataProjectInfo;
    }

    public void setMetadataProjectInfo(MetadataProject metadataProjectInfo) {
        this.metadataProjectInfo = metadataProjectInfo;
    }

    public Map<String, GspMetadata> getMetadataCache() {
        return metadataCache;
    }

    public void setMetadataCache(
            Map<String, GspMetadata> metadataCache) {
        this.metadataCache = metadataCache;
    }

    public List<ModuleGenerateInfo> getModuleInfoList() {
        return moduleInfoList;
    }

    public void setModuleInfoList(
            List<ModuleGenerateInfo> moduleInfoList) {
        this.moduleInfoList = moduleInfoList;
    }

    public ModuleGenerator getApiModuleGenerator() {
        return apiModuleGenerator;
    }

    public void setApiModuleGenerator(ModuleGenerator apiModuleGenerator) {
        this.apiModuleGenerator = apiModuleGenerator;
    }

    public ModuleGenerator getCoreModuleGenerator() {
        return coreModuleGenerator;
    }

    public void setCoreModuleGenerator(ModuleGenerator coreModuleGenerator) {
        this.coreModuleGenerator = coreModuleGenerator;
    }

    public GspMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(GspMetadata metadata) {
        this.metadata = metadata;
    }

    public String getCodePath() {
        return codePath;
    }

    public void setCodePath(String codePath) {
        this.codePath = codePath;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public Map<String, Object> getExtendProperty() {
        return extendProperty;
    }

    public void setExtendProperty(Map<String, Object> extendProperty) {
        this.extendProperty = extendProperty;
    }

    public void setMetadataCache(String metadataid, GspMetadata metadata) {
        metadataCache.put(metadataid, metadata);
    }

    public GspMetadata getMetadataCache(String metadataid) {
        return metadataCache.get(metadataid);
    }

    public GspMetadata getMetadata(String metadataID) {
        if (metadataID.equals(null) || metadataID.equals("")) {
            throw new RuntimeException("metadataID的值为空，请检查依赖。");
        }
        if (metadataCache != null) {
            for (Map.Entry<String, GspMetadata> entry : metadataCache.entrySet()) {
                if (entry.getKey().equals(metadataID)) {
                    return entry.getValue();
                }
            }

            GspMetadata metadata = refCommonService.getRefMetadata(metadataID);
            if (!metadataCache.containsKey(metadataID)) {
                metadataCache.put(metadataID, metadata);
            }

            return metadata;
        } else {
            return null;
        }
    }
}
