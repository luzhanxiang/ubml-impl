package org.openatom.ubml.model.vo.definition.common.mapping;

import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;

/**
 * The Definition Of View Model Object Mapping
 *
 * @ClassName: GspVoObjectMapping
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoObjectMapping extends ViewModelMapping {
    private GspVoObjectSourceType type =
            GspVoObjectSourceType.forValue(0);

    /**
     * 数据源（be或qo或vo）
     */
    public GspVoObjectSourceType getSourceType() {
        return type;
    }

    public void setSourceType(GspVoObjectSourceType value) {
        if (value == GspVoObjectSourceType.QoObject) {
            throw new RuntimeException("Qo类型映射，请使用GspQoObjectMapping。");
        }
        type = value;
    }

    public GspVoObjectMapping clone() {
        return (GspVoObjectMapping)super.clone();
    }
}