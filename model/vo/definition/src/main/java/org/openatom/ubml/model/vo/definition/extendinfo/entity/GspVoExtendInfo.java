package org.openatom.ubml.model.vo.definition.extendinfo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@Entity
@Table(name = "GspViewModel")
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GspVoExtendInfo {

    public GspVoExtendInfo(String id, String extendInfo, String configId, String createdBy,
        Date createdOn, String lastChangedBy, Date lastChangedOn) {
        this.id = id;
        this.extendInfo = extendInfo;
        this.configId = configId;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = lastChangedOn;
    }

    public GspVoExtendInfo(String id, String extendInfo, String configId, String createdBy,
        Date createdOn, String lastChangedBy, Date lastChangedOn, String beSourceId) {
        this(id,extendInfo,configId,createdBy,createdOn,lastChangedBy,lastChangedOn);
        this.beSourceId = beSourceId;
    }

    @Id
    private String id;
    private String extendInfo;
    private String configId;
    private String createdBy;
    private Date createdOn;
    private String lastChangedBy;
    private Date lastChangedOn;
    private String beSourceId;

}
