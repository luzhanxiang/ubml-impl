package org.openatom.ubml.model.vo.definition.common;

/**
 * The Definition Of View Model Parameter Collection Type
 *
 * @ClassName: VMCollectionParameterType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum VMCollectionParameterType
{
	/** 
	 未使用集合形式
	 
	*/
	None,
	/** 
	 列表
	 
	*/
	List,
	/** 
	 数组
	 
	*/
	Array;

	public int getValue()
	{
		return this.ordinal();
	}

	public static VMCollectionParameterType forValue(int value)
	{
		return values()[value];
	}
}