package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

/**
 * The Json Serializer Of View Model Mapping
 *
 * @ClassName: ViewModelMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelMappingSerializer extends JsonSerializer<ViewModelMapping> {


    @Override
    public void serialize(ViewModelMapping mapping, JsonGenerator writer, SerializerProvider serializers) {
        if (mapping == null)
            return;

        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.MapType, mapping.getMapType().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetMetadataId, mapping.getTargetMetadataId());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetMetadataPkgName, mapping.getTargetMetadataPkgName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetObjId, mapping.getTargetObjId());
        //WritePropertyValue(writer, ViewModelJsonConst.TargetMetadataType, mapping.TargetMetadataType.ToString());
        //扩展模型属性
        writeExtendMappingProperty(writer, mapping);

        SerializerUtils.writeEndObject(writer);
    }

    protected void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
    }
}
