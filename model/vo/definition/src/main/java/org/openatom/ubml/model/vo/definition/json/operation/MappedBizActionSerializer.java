package org.openatom.ubml.model.vo.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.vo.definition.action.MappedBizAction;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;
/**
 * The Json Serializer Of The View Model Action Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionSerializer extends VmActionSerializer<MappedBizAction> {
    @Override
    protected VmParameterSerializer getParaConvertor() {
        return new MappedBizActionParaSerializer();
    }

    @Override
    protected void writeExtendOperationProperty(JsonGenerator writer, ViewModelAction op) {

    }
}
