package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
/**
 * The Definition Of Data Mapping Action
 *
 * @ClassName: DataMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DataMappingAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "42221ca3-9ee4-40af-89d2-ff4c8b466ac3";
	public static final String code = "DataMapping";
	public static final String name = "内置数据Mapping操作";
	public DataMappingAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}