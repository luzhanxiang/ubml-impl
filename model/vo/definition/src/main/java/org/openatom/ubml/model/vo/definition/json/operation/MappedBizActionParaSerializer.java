package org.openatom.ubml.model.vo.definition.json.operation;

import org.openatom.ubml.model.vo.definition.action.mappedbiz.MappedBizActionParameter;
/**
 * The Json  Serializer Of The View Model Action Parameter Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionParaSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionParaSerializer extends VmParameterSerializer<MappedBizActionParameter> {
}
