package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.MappingType;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Deserializer Of View Model Mapping
 *
 * @ClassName: ViewModelMappingDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelMappingDeserializer extends JsonDeserializer<ViewModelMapping> {
    @Override
    public ViewModelMapping deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        ViewModelMapping viewModelMapping = createVmMapping();

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(viewModelMapping, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return viewModelMapping;
    }

    private void readPropertyValue(ViewModelMapping mapping, String propName, JsonParser reader) {
        switch (propName) {
            case ViewModelJsonConst.MapType:
                mapping.setMapType(SerializerUtils.readPropertyValue_Enum(reader, MappingType.class, MappingType.values(), MappingType.BizEntity));
                break;
            case ViewModelJsonConst.TargetMetadataId:
                mapping.setTargetMetadataId(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.TargetMetadataPkgName:
                mapping.setTargetMetadataPkgName(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.TargetObjId:
                mapping.setTargetObjId(SerializerUtils.readPropertyValue_String(reader));
                break;
            //case ViewModelJsonConst.TargetMetadataType:
            //	mapping.TargetMetadataType = SerializerUtils.ReadEnum(reader, MetadataType.BizEntity);
            //	break;
            default:
                if (!readExtendMappingProperty(reader, mapping, propName)) {
                    throw new RuntimeException("未定义的mapping属性：" + propName);
                }
                break;
        }
    }

    protected boolean readExtendMappingProperty(JsonParser reader, ViewModelMapping mapping, String propertyName) {
        return false;
    }

    protected ViewModelMapping createVmMapping() {
        return new ViewModelMapping();
    }
}
