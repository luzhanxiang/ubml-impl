package org.openatom.ubml.model.vo.definition.common.mapping;

/**
 * The Definition Of  View Model Object Source Type
 *
 * @ClassName: GspVoObjectSourceType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspVoObjectSourceType
{
	/** 
	 BE 
	 
	*/
	BeObject,

	/** 
	 QO
	 
	*/
	QoObject,

	/** 
	 VO
	 
	*/
	VoObject;

	public int getValue()
	{
		return this.ordinal();
	}

	public static GspVoObjectSourceType forValue(int value)
	{
		return values()[value];
	}
}