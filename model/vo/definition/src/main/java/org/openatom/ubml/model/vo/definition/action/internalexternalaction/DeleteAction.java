package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
/**
 * The Definition Of Delete Action
 *
 * @ClassName: DeleteAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DeleteAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "9a17e935-7366-489d-b110-0ae103e5648e";
	public static final String code= "Delete";
	public static final String name = "内置删除操作";
	public DeleteAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}