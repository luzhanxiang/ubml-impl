package org.openatom.ubml.model.vo.definition.common;

/**
 * The Definition Of View Model Help Config
 *
 * @ClassName: VMHelpConfig
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VMHelpConfig implements Cloneable
{
	private String helpId;
	public final String getHelpId()
	{
		return helpId;
	}
	public final void setHelpId(String value)
	{
		helpId = value;
	}

//	public final Object clone()
	public Object clone()
	{
		VMHelpConfig config = new VMHelpConfig();
		config.helpId = helpId;
		return config;
	}
}