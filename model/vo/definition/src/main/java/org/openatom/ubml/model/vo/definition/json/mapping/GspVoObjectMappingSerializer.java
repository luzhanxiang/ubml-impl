package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoObjectMapping;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

/**
 * The  Josn Serializer Of View Model Object Mapping
 *
 * @ClassName: GspVoObjectMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoObjectMappingSerializer extends ViewModelMappingSerializer {

    @Override
    protected final void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
        GspVoObjectMapping voMapping = (GspVoObjectMapping) mapping;
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.SourceType, voMapping.getSourceType().toString());
        //扩展模型属性
        writeExtendObjMappingProperty(writer, mapping);
    }

    protected void writeExtendObjMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
    }
}


