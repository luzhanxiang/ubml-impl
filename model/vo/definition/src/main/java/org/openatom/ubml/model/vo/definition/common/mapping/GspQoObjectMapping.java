package org.openatom.ubml.model.vo.definition.common.mapping;

public class GspQoObjectMapping extends GspVoObjectMapping
{
	/**
	 * The Definition Of The Qo Object Mapping
	 *
	 * @ClassName: GspVoObjectSourceType
	 * @Author: Benjamin Gong
	 * @Date: 2021/1/11 17:13
	 * @Version: V1.0
	 */
	public GspVoObjectSourceType SourceType =
		GspVoObjectSourceType.QoObject;

	private String privateIndexVoId;
	public final String getIndexVoId()
	{
		return privateIndexVoId;
	}
	public final void setIndexVoId(String value)
	{
		privateIndexVoId = value;
	}
}