package org.openatom.ubml.model.vo.definition.collection;

import java.io.Serializable;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonObject;
import org.openatom.ubml.model.common.definition.commonmodel.collection.GspObjectCollection;
import org.openatom.ubml.model.vo.definition.GspViewObject;

/**
 * The Collection Of View Model Object
 *
 * @ClassName: ViewObjectCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewObjectCollection extends GspObjectCollection implements Serializable
{
	/** 
	 结点集合构造函数
	 
	 @param parentObject 父节点
	*/
//	public ViewObjectCollection(IGSPCommonObject parentObject)
	public ViewObjectCollection(IGspCommonObject parentObject)
	{
		super(parentObject);
	}

	/** 
	 根据ID获取节点
	 
	 @param id 节点ID
	 @return 节点
	*/
	public final GspViewObject getItem(String id)
	{
//		Object tempVar = this.FirstOrDefault(i => id.equals(i.ID));
		for ( IGspCommonObject item : this){
			if(item.getID().equals(id)){
				IGspCommonObject tempVar=item;
				return (GspViewObject)((tempVar instanceof GspViewObject) ? tempVar : null);
			}
		}
		return null;

	}

	/** 
	 重载Equals方法
	 
	 @param obj 要比较的对象
	 @return 如果当前对象等于 other 参数，则为 true；否则为 false。
	*/
	@Override
	public boolean equals(Object obj)
	{
		if ( obj.equals(null))
		{
			return false;
		}
		if ( obj.equals(this))
		{
			return true;
		}
		if (obj.getClass() != getClass())
		{
			return false;
		}

		return equals((ViewObjectCollection)obj);
	}

	/** 
	 获取HashCode
	 
	 @return hashCode
	*/
	@Override
	public int hashCode()
	{
		return super.hashCode();
	}

	/** 
	 当前对象是否等于同一类型的另一个对象。
	 
	 @param other 与此对象进行比较的对象。
	 @return 如果当前对象等于 other 参数，则为 true；否则为 false。
	*/
	protected boolean equals(ViewObjectCollection other)
	{
//		if (Count != other.size())
		if (this.size() != other.size())
		{
			return false;
		}
//		for (var item : this)
		for (IGspCommonObject item : this)
		{
			IGspCommonObject otherItem = other.getItem(item.getID());
			if (otherItem == null)
			{
				return false;
			}
			if (!item.equals(otherItem))
			{
				return false;
			}
		}

		return true;
	}

	/** 
	 克隆
	 
	 @return VO节点元素集合
	*/
//	public final Object clone()
	public final ViewObjectCollection clone()
	{
//		ViewObjectCollection collections = new ViewObjectCollection(ParentObject);
		ViewObjectCollection collections = new ViewObjectCollection(getParentObject());
//		for (var node : this)
		for (IGspCommonObject node : this)
		{
			Object tempVar = ((GspViewObject)((node instanceof GspViewObject) ? node : null)).clone();
			collections.add((GspViewObject)((tempVar instanceof GspViewObject) ? tempVar : null));
		}

		return collections;
	}

}