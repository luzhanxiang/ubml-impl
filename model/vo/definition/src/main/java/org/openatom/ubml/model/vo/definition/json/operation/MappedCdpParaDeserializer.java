package org.openatom.ubml.model.vo.definition.json.operation;

import org.openatom.ubml.model.vo.definition.action.mappedcdp.MappedCdpActionParameter;
/**
 * The Json Deserializer Of Mapped Component Action Parameter Definition
 *
 * @ClassName: MappedCdpParaDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpParaDeserializer extends VmParameterDeserializer<MappedCdpActionParameter> {
    @Override
    protected MappedCdpActionParameter createVmPara() {
        return new MappedCdpActionParameter();
    }
}
