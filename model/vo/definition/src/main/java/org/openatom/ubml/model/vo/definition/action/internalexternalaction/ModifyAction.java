package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
/**
 * The  Definition Of Modify Action
 *
 * @ClassName: ModifyAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ModifyAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "47dd3752-72a3-4c56-81c0-ae8ccfe5eb98";
	public static final String code = "Modify";
	public static final String name = "内置修改操作";
	public ModifyAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}

}