package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspQoObjectMapping;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

/**
 * The Josn Serializer Of QO Object Mapping Definition
 *
 * @ClassName: GspQoObjectMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspQoObjectMappingSerializer extends ViewModelMappingSerializer {

    @Override
    protected final void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
        GspQoObjectMapping voMapping = (GspQoObjectMapping) mapping;
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IndexVoId, voMapping.getIndexVoId());
    }
}
