package org.openatom.ubml.model.vo.definition.common.copy;

import org.openatom.ubml.model.vo.definition.GspViewModel;

/**
 * The Context While Copying
 *
 * @ClassName: CopyContext
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CopyContext
{
	/** 
	 源Vo
	 
	*/
	private GspViewModel privateOriginVo;
	public final GspViewModel getOriginVo()
	{
		return privateOriginVo;
	}
	public final void setOriginVo(GspViewModel value)
	{

		privateOriginVo = value;
	}

	/** 
	 新Vo的元数据头节点
	 
	*/
//	private MetadataHeader privateMetadataHeader;
	private CopyContext privateMetadataHeader;
	public final CopyContext getMetadataHeader()
	{

		return privateMetadataHeader;
	}
	public final void setMetadataHeader(CopyContext value)
	{
		privateMetadataHeader = value;
	}

	private boolean includeVoActions = false;
	/** 
	 是否拷贝Vo操作
	 
	*/
	 boolean getIncludeVoActions(){
	 	return this.includeVoActions;
	 }
	public final void setIncludeVoActions(boolean value)
	{
		if (value)
		{
			throw new RuntimeException("vo元数据拷贝暂不支持拷贝操作。");
		}
		includeVoActions = value;
	}
}