package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;

/**
 * The Definition Of Create Action
 *
 * @ClassName: CreateAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CreateAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "52479451-8e22-4751-8684-80489ce5786b";
	public static final String code = "Create";
	public static final String name = "内置新增数据操作";
	public CreateAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}